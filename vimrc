" set encoding for opened files
set encoding=utf-8

"if &term == "screen"
"    set t_Co=256
"elseif &term == "xterm"
"    set t_Co=256
"endif

" automatic reloading of vimrc file
autocmd! bufwritepost .vimrc source %

" pathogen
set nocp  " pathogen needs to run in nocompatible mode
call pathogen#infect()
syntax on
filetype plugin indent on

let mapleader = ","

set background=light
colorscheme solarized

if exists("g:portable")
    let s:vimrcpython = printf("%s/vimrc-python", g:portable)
    exec 'source '.fnameescape(s:vimrcpython)
else
    source ~/.vim/vimrc-python
endif

set softtabstop=4 " backspace all 4 spaces at once

" open NERDTree when Vim starts
autocmd vimenter * NERDTree
" shortcut to toggle NERDTree on/off
"map <C-n> :NERDTreeToggle<CR>
map <Leader>d :NERDTreeToggle<CR>

set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4

" mouse as in graphic environment
set mouse=a

" """""""""""""""""""""""""""""
" syntastic checker
" """""""""""""""""""""""""""""
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
nnoremap <Leader>e :SyntasticCheck<CR> :SyntasticToggleMode<CR>


""""""""""""""""""""""""""""""
" airline
" """"""""""""""""""""""""""""""
let g:airline_theme             = 'powerlineish'
let g:airline_enable_branch     = 1
let g:airline_enable_syntastic  = 1
set laststatus=2 " appear always, not only when new split is created
" " vim-powerline symbols
" "let g:airline_left_sep          = '⮀'
" "let g:airline_left_alt_sep      = '⮁'
" "let g:airline_right_sep         = '⮂'
" "let g:airline_right_alt_sep     = '⮃'
" "let g:airline_branch_prefix     = '⭠'
" "let g:airline_readonly_symbol   = '⭤'
" "let g:airline_linecolumn_prefix = '⭡'
"
